﻿using System.Web;
using System.Web.Mvc;

namespace Midterm_Lab_1460987
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
