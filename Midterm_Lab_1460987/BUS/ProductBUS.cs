﻿using ConnShop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Midterm_Lab_1460987.BUS
{
    public class ProductBUS
    {
        public static IEnumerable<tbl_product> List()
        {
            var db = new ConnShopDB();
            return db.Query <tbl_product>("select * from tbl_product");

        }
        public static tbl_product GetbyID(int id)
        {
            var db = new ConnShopDB();
            return db.SingleOrDefault<tbl_product>("select * from tbl_product where product_id =@0", id);
        }
        public static void AddProduct(ConnShop.tbl_product sp)
        {
            var a = new ConnShopDB();
            a.Insert(sp);
        }
        public static void Xoa(ConnShop.tbl_product id)
        {
            var a = new ConnShopDB();
            a.Delete(id);
        }
    }
}